#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#ifndef CARDS_H
#define CARDS_H

using namespace std;

enum suit_t {OROS, COPAS, ESPADAS, BASTOS};

/*
 The values for this type start at 0 and increase by one
 afterwards until they get to SIETE.
 The rank reported by the function Card::get_rank() below is
 the value listed here plus one.
 E.g:
 The rank of AS is reported as    static_cast<int>(AS) + 1   = 0 + 1 =  1
 The rank of SOTA is reported as  static_cast<int>(SOTA) + 1 = 9 + 1 = 10
 */
enum rank_t {AS, DOS, TRES, CUATRO, CINCO, SEIS, SIETE, SOTA=9, CABALLO=10, REY=11};

class Card {
public:
    // Constructor assigns random rank & suit to card.
    Card();
    
    // Accessors
    string get_spanish_suit() const;
    string get_spanish_rank() const;
    
    /*
     These are the only functions you'll need to code
     for this class. See the implementations of the two
     functions above to get an idea of how to proceed.
     */
    string get_english_suit() const;
    string get_english_rank() const;
    
    // Converts card rank to number.
    // The possible returns are: 1, 2, 3, 4, 5, 6, 7, 10, 11 and 12
    double get_value() const;
    
    // Compare rank of two cards. E.g: Eight<Jack is true.
    // Assume Ace is always 1.
    // Useful if you want to sort the cards.
    bool operator < (Card card2) const;
    
private:
    suit_t suit;
    rank_t rank;
};








class Hand
{
public:
    Hand()
    {
        m_total_value = 0;
        m_num_cards = 0;
    }
    
    void get_card()
    {
        Card new_card;
        m_hand.push_back(new_card);
        m_num_cards ++;
        
        double value;
        value = new_card.get_value();
        m_total_value += value;
        
    }
    
    double get_total_value()
    {
        return m_total_value;
    }
    
    int get_num_cards()
    {
        return m_num_cards;
    }
    
    
    vector<Card> return_cards()
    {
        return m_hand;
    }
    
    
private:
    vector<Card> m_hand;
    double m_total_value;
    int m_num_cards;
    
    
};




class Player
{
public:
    Player(int money)
    {
        m_player_money = money;
    }
    
    double player_get_total()
    {
        return player_hand.get_total_value();
    }
    
    void player_get_card()
    {
        player_hand.get_card();
    }
    
    int get_money()
    {
        return m_player_money;
    }
    
    
    void lose_money(int bet)
    {
        m_player_money-= bet;
    }
    
    void win_money(int bet)
    {
        m_player_money+= bet;
    }
    
    bool lose_match()
    {
        if (m_player_money <= 0)
            return true;
        else
            return false;
    }
    
    void player_print_cards()
    {
        vector<Card> a = player_hand.return_cards();
        for (int i =0 ; i < a.size(); i++)
        {
            cout << "       " << a[i].get_spanish_suit() << " de " << a[i].get_spanish_rank() << endl;
        }
    }
    
    
    
private:
    Hand player_hand;
    int m_player_money;
};


class Dealer
{
public:
    Dealer()
    {
        dealer_money_losed = 0;
    }
    
    double dealer_get_total()
    {
        return dealer_hand.get_total_value();
    }
    
    void get_card()
    {
        dealer_hand.get_card();
    }
    
    void lose_money()
    {
        dealer_money_losed++;
    }
    
    bool lose_match()
    {
        if(dealer_money_losed >= 900)
            return true;
        else
            return false;
    }
    
    void dealer_print_cards()
    {
        vector<Card> a = dealer_hand.return_cards();
        for (int i =0 ; i < a.size(); i++)
        {
            cout << a[i].get_spanish_suit() << " de " << a[i].get_spanish_rank() << endl;
        }
    }
    
    
    
private:
    Hand dealer_hand;
    int dealer_money_losed;
};





















#endif
